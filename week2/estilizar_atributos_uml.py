new_tokens = []

with open("input.txt", 'r') as file:
    #Leer linea por linea
    for line in file:
        #Quitar espacios por ambos lados y salto de linea al final(/n)
        line = line.strip()
        #Descomponer en tokens
        tokens = line.split();
        #print(tokens)
        #Analizar tokens, a mi solo me interesa los primeros tres tokens
        #Primer token cambias private por -
        #Segundo token es el tipo de dato
        #Tercer token es el nombre
        new_tokens.append( ['- ', tokens[2], ": ", tokens[1] ] )


with open('ouput.txt', 'w') as file:
    for item in new_tokens:
        line = "".join(item)
        file.write(line + '\n')



